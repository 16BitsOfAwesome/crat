package org.impactable.cowduckfever;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class CowDuckFever extends Game {

	@Override
	public void create () {
		GameScreen gameScreen = new GameScreen();
		setScreen(gameScreen);
	}
}
