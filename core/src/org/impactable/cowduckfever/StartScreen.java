package org.impactable.cowduckfever;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by James on 4/17/2016.
 */
public class StartScreen {
    Texture space, up, w, enter, trophy;

    BitmapFont font;
    GlyphLayout glyphLayout;

    String jump = "JUMP";
    String change = " - CHANGE";
    String start = " - START!";

    private int bestScore = 0;

    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }

    public StartScreen(Texture space, Texture up, Texture w, Texture enter, Texture trophy, BitmapFont font, GlyphLayout smallGlyphLayout) {
        this.trophy = trophy;
        this.space = space;
        this.up = up;
        this.w = w;
        this.enter = enter;
        this.font = font;
        this.glyphLayout = smallGlyphLayout;
    }

    public void render(SpriteBatch batch){
        batch.draw(up, 300f, 300f);
        batch.draw(w, 355f, 300f);

        glyphLayout.setText(font, jump);
        font.draw(batch, glyphLayout, 440f, 300f + 55);

        batch.draw(space, 305f, 245f);

        glyphLayout.setText(font, change);
        font.draw(batch, glyphLayout, 450f, 245f + 55);

        batch.draw(enter, 305f, 150f);
        glyphLayout.setText(font, start);
        font.draw(batch, glyphLayout, 405f, 150 + 55);
    }

    public void renderHighScore(SpriteBatch batch){
        batch.draw(trophy, 15, 340f);
        glyphLayout.setText(font, "" + bestScore);
        font.draw(batch, glyphLayout, 50, 370f);
    }
}
