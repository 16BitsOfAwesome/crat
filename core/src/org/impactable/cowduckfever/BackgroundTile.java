package org.impactable.cowduckfever;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by James on 4/16/2016.
 */
public class BackgroundTile {
    private final Rectangle shape;
    private final Texture texture;
    private final float floorLevel;

    public static final int BG_TILE_SIZE = 256;
    public static final float SPEED = Wall.MOVEMENT_SPEED;

    public BackgroundTile(Texture texture, float floorLevel) {
        this.shape = new Rectangle(0, 0, BG_TILE_SIZE, BG_TILE_SIZE);
        this.texture = texture;
        this.floorLevel = floorLevel;
    }

    public void draw(SpriteBatch batch){
        batch.draw(texture, shape.x, floorLevel - 46, BG_TILE_SIZE, BG_TILE_SIZE);
    }

    public void setPosition(float x){
        shape.setX(x);
    }

    public void update(float delta){
        setPosition(shape.x - (SPEED * delta));
    }

    public float getX() {
        return shape.getX();
    }
}
