package org.impactable.cowduckfever;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by James on 4/16/2016.
 */
public class Cloud {
    private final Rectangle shape;
    private final TextureRegion texture;

    public static final int BG_TILE_SIZE = 128;
    public float SPEED = Wall.MOVEMENT_SPEED / 13;

    public Cloud(Texture texture) {
        this.shape = new Rectangle(0, 235, BG_TILE_SIZE, BG_TILE_SIZE);
        TextureRegion[][] textures = TextureRegion.split(texture, BG_TILE_SIZE, BG_TILE_SIZE);

        int selected = MathUtils.random(textures[0].length - 1);

        this.texture = textures[0][selected];

        if (selected == 0 || selected == 2 || selected == 5){
            SPEED *= 0.8;
        }
    }

    public void draw(SpriteBatch batch){
        batch.draw(texture, shape.x, shape.y, BG_TILE_SIZE, BG_TILE_SIZE);
    }

    public void drawDebug(ShapeRenderer shapeRenderer){
            shapeRenderer.rect(shape.x, shape.y, shape.width, shape.height);
    }

    public void setPosition(float x){
        shape.setX(x);
    }

    public void update(float delta){
        setPosition(shape.x - (SPEED * delta));
    }

    public float getX() {
        return shape.getX();
    }

    public float getWidth() {
        return shape.getWidth();
    }
}
