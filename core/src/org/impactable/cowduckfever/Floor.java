package org.impactable.cowduckfever;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by James on 4/16/2016.
 */
public class Floor {
    private final Rectangle collisionShape;

    public Floor(float width, float height) {
        this.collisionShape = new Rectangle(0, 0, width, height);
    }

    public void draw(ShapeRenderer renderer){
        renderer.setColor(0.21875f, 0.1015625f, 0.015625f, 1);
        renderer.rect(collisionShape.x, collisionShape.y, collisionShape.getWidth(), collisionShape.getHeight());
    }

    public void drawDebug(ShapeRenderer shapeRenderer){
        shapeRenderer.rect(collisionShape.x, collisionShape.y, collisionShape.x + collisionShape.width, collisionShape.y + collisionShape.height);
    }

    public boolean isAnimalColliding(CowDuck cowDuck) {
        Circle animalCollisionShape = cowDuck.getCollisionShape();
        return Intersector.overlaps(animalCollisionShape, collisionShape) || (animalCollisionShape.y < collisionShape.getY());
    }

    public float getHeight(){
        return collisionShape.getHeight();
    }
}
