package org.impactable.cowduckfever;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by James on 4/16/2016.
 */
public class GameScreen extends ScreenAdapter {
    private BitmapFont bitmapFont;
    private GlyphLayout glyphLayout;

    private ShapeRenderer shapeRenderer;
    private Viewport viewport;
    private Camera camera;
    private SpriteBatch batch;

    private CowDuck cowDuck;

    private Floor floor;
    private final Array<Wall> walls = new Array<>();
    private final Array<BackgroundTile> bgTiles = new Array<BackgroundTile>();
    private final Array<Cloud> clouds = new Array<Cloud>();

    private float nextCloudTime = 0f;

    private SafeZone safeZone;

    public static final float UNIT_SIZE = 64f;

    private static final float WORLD_WIDTH = 720f;
    private static final float WORLD_HEIGHT = 480f;

    private boolean showDebugging = false;
    private boolean showSafeZone = false;

    private static float GAP_BETWEEN_WALLS_DIFFICULT = 350f;
    private static float GAP_BETWEEN_WALLS_EASY = 500f;
    private static float GAP_BETWEEN_WALLS_EASIER_BUMP = 25f;
    private static float MILESTONE = 10f; // gaps
    private float nextGapSize = GAP_BETWEEN_WALLS_EASY;
    private int score = 0;
    private int bestScore = 0;

    private final AssetManager assetManager = new AssetManager();

    private Texture bgTexture;
    private Texture cloudTexture;
    private Texture trophyTexture;

    boolean showStartScreen = true;
    private boolean firstOpen = true;
    private StartScreen startScreen;
    private float initialDelay = 2f;

    private int gauntlet = 0;

    private long lastCowDuckTimeFrame = 0;
    private static final float frameInMilliseconds = 1000f / 60;


    public GameScreen() {
        assetManager.load("cow.png", Texture.class);
        assetManager.load("duck.png", Texture.class);
        assetManager.load("bg.png", Texture.class);
        assetManager.load("clouds.png", Texture.class);
        assetManager.load("trophy.png", Texture.class);
        assetManager.load("trophy-small.png", Texture.class);
        assetManager.load("key-enter.png", Texture.class);
        assetManager.load("key-space.png", Texture.class);
        assetManager.load("key-up.png", Texture.class);
        assetManager.load("key-w.png", Texture.class);

        lastCowDuckTimeFrame = TimeUtils.millis();

        assetManager.finishLoading();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void render(float delta) {
        updateDebug(delta);

        clearScreen();
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        floor.draw(shapeRenderer);

        shapeRenderer.end();

        batch.begin();

        for(BackgroundTile tile : bgTiles){
            tile.draw(batch);
        }

        for (Cloud cloud : clouds){
            cloud.draw(batch);
        }

        if (!showStartScreen) {
            for (Wall wall : walls) {
                wall.drawTrophy(batch);
            }
        }

        cowDuck.draw(batch);

        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        if (!showStartScreen) {

            shapeRenderer.setColor(Color.FIREBRICK);
            for (Wall wall : walls) {
                wall.draw(shapeRenderer);
            }

        }
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        shapeRenderer.setColor(Color.YELLOW);

        if (showDebugging) {
            cowDuck.drawDebug(shapeRenderer);
            floor.drawDebug(shapeRenderer);

            for (Wall wall : walls){
                wall.drawDebug(shapeRenderer);
            }

            for (Cloud cloud : clouds){
                cloud.drawDebug(shapeRenderer);
            }
        }

        if (showSafeZone){
            safeZone.draw(shapeRenderer);
        }
        shapeRenderer.end();

        batch.begin();

        if (showStartScreen) {
            startScreen.render(batch);
        }

        if (!showStartScreen) {
            drawScore();
        } else {
            if (!firstOpen){
                drawScore();
                startScreen.renderHighScore(batch);
            }
        }

        batch.end();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(0.4609375f, 0.69921875f, 0.74609375f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void show() {
        Texture cowTexture = assetManager.get("cow.png");
        Texture duckTexture = assetManager.get("duck.png");
        bgTexture = assetManager.get("bg.png");
        cloudTexture = assetManager.get("clouds.png");
        trophyTexture = assetManager.get("trophy.png");
        Texture trophySmallTexture = assetManager.get("trophy-small.png");
        Texture upTexture = assetManager.get("key-up.png");
        Texture wTexture = assetManager.get("key-w.png");
        Texture enterTexture = assetManager.get("key-enter.png");
        Texture spaceTexture = assetManager.get("key-space.png");

        cowDuck = new CowDuck(cowTexture, duckTexture);

        camera = new OrthographicCamera(WORLD_WIDTH, WORLD_HEIGHT);
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
        viewport.apply(true);
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        bitmapFont = new BitmapFont(Gdx.files.internal("font.fnt"));
        BitmapFont smallBitmapFont = new BitmapFont(Gdx.files.internal("font-small.fnt"));
        glyphLayout = new GlyphLayout();
        GlyphLayout smallGlyphLayout = new GlyphLayout();

        safeZone = new SafeZone(WORLD_WIDTH, WORLD_HEIGHT);
        floor = new Floor(WORLD_WIDTH, UNIT_SIZE);
        cowDuck.setPosition((((WORLD_WIDTH / 2) - UNIT_SIZE) / 2) + UNIT_SIZE, WORLD_HEIGHT / 3);

        for (int i = 0; i < 6; i++) {
            BackgroundTile backgroundTile = new BackgroundTile(bgTexture, floor.getHeight());
            backgroundTile.setPosition(i * 256f);
            bgTiles.add(backgroundTile);
        }

        for (int i = 0; i < 4; i++) {
            Cloud cloud = new Cloud(cloudTexture);
            cloud.setPosition(MathUtils.random(128, WORLD_WIDTH - 128));
            clouds.add(cloud);
        }

        startScreen = new StartScreen(spaceTexture, upTexture, wTexture, enterTexture, trophySmallTexture, smallBitmapFont, smallGlyphLayout);

        Preferences preferences = Gdx.app.getPreferences(CowDuck.class.getName());
        bestScore = preferences.getInteger("bestScore", 0);
        //         preferences.remove("bestScore");
    }

    private void update(float delta){ update(delta, false); }
    private void updateDebug(float delta){ update(delta, true); }

    private void update(float gameDelta, boolean debuggingEnabled) {
        long timeSinceLastFrame = TimeUtils.timeSinceMillis(lastCowDuckTimeFrame);
        float delta = timeSinceLastFrame / 1000f;

        if (initialDelay > 0){
            initialDelay -= delta;
        }

        Input input = Gdx.input;

        if (input.isKeyJustPressed(Input.Keys.SPACE) || touchedLeftSide(input)){
            cowDuck.shapeShift();
            if (!cowDuck.airborne) {
                cowDuck.resetPosition(floor.getHeight());
            }
        }

        if (input.isKeyJustPressed(Input.Keys.ENTER) || input.justTouched()){
            if (showStartScreen) {
                restart();
            }
            showStartScreen = false;
            firstOpen = false;
        }

        if (input.isKeyJustPressed(Input.Keys.UP) || input.isKeyJustPressed(Input.Keys.W) || touchedRightSide(input)){
            cowDuck.jump();
        }

//        if (input.isKeyJustPressed(Input.Keys.S)){
//            showSafeZone = !showSafeZone;
//        }

        if (input.isKeyJustPressed(Input.Keys.D) && debuggingEnabled){
            showDebugging = !showDebugging;
        }

        if (timeSinceLastFrame >= frameInMilliseconds) {
            lastCowDuckTimeFrame = TimeUtils.millis();

            if (!showStartScreen) {
                updateWalls(delta);
                updateScore();

                if (handleWallCollision()){
                    startScreen.setBestScore(bestScore);
                    showStartScreen = true;

                    Preferences preferences = Gdx.app.getPreferences(CowDuck.class.getName());
                    preferences.putInteger("bestScore", bestScore);
                    preferences.flush();
                }
            }

            cowDuck.update(delta);
            handleFloorCollision();
            updateBG(delta);
            updateClouds(delta);
        }
    }

    private boolean touchedRightSide(Input input) {
        boolean touched = false;

        if (input != null && input.justTouched()) {
            int x = input.getX();
            touched = (x > WORLD_WIDTH / 2);
        }

        return touched;
    }

    private boolean touchedLeftSide(Input input) {
        boolean touched = false;

        if (input != null && input.justTouched()) {
            int x = input.getX();
            touched = (x <= WORLD_WIDTH / 2);
        }

        return touched;
    }

    private void updateClouds(float delta) {
        Array<Cloud> cloudsToRemove = new Array<Cloud>();

        for (Cloud cloud : clouds){
            if (cloud.getX() < -cloud.getWidth()){
                cloudsToRemove.add(cloud);
            } else {
                cloud.update(delta);
            }
        }

        clouds.removeAll(cloudsToRemove, true);

        nextCloudTime -= delta;

        if (nextCloudTime <= 0){
            Cloud cloud = new Cloud(cloudTexture);
            cloud.setPosition(WORLD_WIDTH);
            clouds.add(cloud);

            nextCloudTime = MathUtils.random(3f, 5f);
        }
    }

    private void restart() {
        walls.clear();
        score = 0;
        bested = (bestScore == 0);
        initialDelay = 2f;
        nextGapSize = GAP_BETWEEN_WALLS_EASY;
        gauntlet = 0;
    }

    private void updateWalls(float delta) {
        for (Wall wall : walls){
            wall.update(delta);
        }

        if (initialDelay <= 0) {
            needNewWall();
            removePassedWalls();
        }
    }

    private void updateBG(float delta) {
        for (BackgroundTile tile : bgTiles){
            tile.update(delta);
        }

        needNewTile();
        removePassedTile();
    }

    private void needNewTile() {
        BackgroundTile tile = bgTiles.peek();

        if (tile.getX() < WORLD_WIDTH){
            buildNewBGTile();
        }
    }

    private void removePassedTile() {
        if (bgTiles.size > 0){
            BackgroundTile firstTile = bgTiles.first();

            if (firstTile.getX() < -BackgroundTile.BG_TILE_SIZE){
                bgTiles.removeValue(firstTile, true);
            }
        }
    }

    private void buildNewBGTile(){
        BackgroundTile tile = new BackgroundTile(bgTexture, floor.getHeight());
        tile.setPosition(bgTiles.peek().getX() + BackgroundTile.BG_TILE_SIZE);
        bgTiles.add(tile);
    }

    private void handleFloorCollision() {
        if (floor.isAnimalColliding(cowDuck) || cowDuck.getPositionY() < floor.getHeight()) {
            cowDuck.resetPosition(floor.getHeight());
        }
    }

    private boolean handleWallCollision() {
        boolean collide = false;
        for (Wall wall : walls){
            if (wall.didAnimalCollide(cowDuck)){
                collide = true;
                break;
            }
        }
        return collide;
    }

    boolean bested = true;

    private void buildNewWall(){
        Wall wall = new Wall(floor.getHeight(), trophyTexture);
        wall.setPosition(WORLD_WIDTH + wall.getWidth());

        if (score >= bestScore && !bested && bestScore > 0){
            wall.setBest(true);
            bested = true;
        }

        walls.add(wall);

        setupNextWall();
    }

    private void setupNextWall() {
        // For every 10 score, make it a bit more difficult
        int milestones = MathUtils.floor(score / MILESTONE);
        float maxGapWidth = GAP_BETWEEN_WALLS_EASY - (milestones * GAP_BETWEEN_WALLS_EASIER_BUMP);
        boolean maxDifficulty = maxGapWidth <= GAP_BETWEEN_WALLS_DIFFICULT;


        if (gauntlet == 0 && MathUtils.randomBoolean(0.10f)) {
            gauntlet = 10;
        }

        if (gauntlet > 0) {
            gauntlet--;
            nextGapSize = GAP_BETWEEN_WALLS_DIFFICULT;

            if (maxDifficulty) {
                nextGapSize -= 50f;
            }

            return;
        }

        if (MathUtils.randomBoolean(0.10f) && !maxDifficulty) {
            nextGapSize = GAP_BETWEEN_WALLS_DIFFICULT;
            return;
        }

        if (!maxDifficulty) {
            nextGapSize = MathUtils.random(GAP_BETWEEN_WALLS_DIFFICULT, maxGapWidth);

            if (milestones < 2) {
                nextGapSize += 100f;
            } else if (milestones < 4) {
                nextGapSize += 50f;
            }
        } else {
            nextGapSize = GAP_BETWEEN_WALLS_DIFFICULT;

            if (MathUtils.randomBoolean(0.25f)) {
                nextGapSize -= 75f;
            }
        }
    }

    private void needNewWall(){
        if (walls.size == 0){
            buildNewWall();
        } else {
            Wall wall = walls.peek();

            if (wall.getX() < WORLD_WIDTH - nextGapSize){
                buildNewWall();
            }
        }
    }

    private void removePassedWalls(){
        if (walls.size > 0){
            Wall firstWall = walls.first();

            if (firstWall.getX() < -firstWall.getWidth() - 65){
                walls.removeValue(firstWall, true);
            }
        }
    }

    private void updateScore(){
        try {
            Wall wall = walls.first();

            if (wall.getX() < cowDuck.getX() && !wall.pointClaimed()) {
                wall.claimPoint();
                score++;

                if (score > bestScore) {
                    bestScore = score;
                }
            }
        } catch (Throwable ignored) {}
    }

    private void drawScore(){
        String scoreString = Integer.toString(score);

        glyphLayout.setText(bitmapFont, scoreString);
        bitmapFont.draw(batch, scoreString, 15, viewport.getWorldHeight() - 15);
    }
}
