package org.impactable.cowduckfever;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * Created by James on 4/16/2016.
 */
public class Wall {
    private static final float COLLISION_RECTANGLE_WIDTH = 5f;
    private static final float COLLISION_RECTANGLE_HEIGHT = Gdx.graphics.getHeight();

    private static final float DUCK_HOLE_SIZE = CowDuck.DUCK_COLLISION_RADIUS * 5f;
    private static final float COW_HOLE_SIZE = CowDuck.COW_COLLISION_RADIUS * 4f;

    private final Array<Rectangle> collisionShapes = new Array<>();

    public static float MOVEMENT_SPEED = 400f;
    private static final float MOVEMENT_SPEED_INCREMENT = 50f;


    private static final int DUCK_RUN = 0;
    private static final int COW_RUN = 1;
    private static final int DUCK_JUMP = 2;
    private static final int COW_JUMP = 3;
    private static final int WALL_OPTIONS = COW_JUMP;

    private static final float DUCK_RUN_HOLE_HEIGHT = CowDuck.DUCK_COLLISION_RADIUS * 3.2f;
    private static final float COW_RUN_HOLE_HEIGHT = CowDuck.COW_COLLISION_RADIUS * 3;
    private static final float DUCK_JUMP_HOLE_HEIGHT = 140f;
    private static final float COW_JUMP_HOLE_HEIGHT = 240f;

    private float floorLevel = 0;

    private boolean isBest = false;

    public static int consecutiveRunHoles = 0;

    public static int consecutiveDuckable = 0;
    public static int consecutiveCowable = 0;


    private boolean pointClaimed = false;

    private final Texture trophyTexture;

    public Wall(float floorLevel, Texture trophyTexture) {
        this.floorLevel = floorLevel;
        this.trophyTexture = trophyTexture;

        if (consecutiveDuckable > 3) {
            constructOneHole();
            consecutiveDuckable = 0;
            return;
        }

        if (consecutiveCowable > 5) {
            constructOneHole();
            consecutiveCowable = 0;
            return;
        }

        boolean oneHole = MathUtils.randomBoolean(0.7f);

        if (oneHole){ // any type of holes
            constructOneHole();
        } else {
            if (MathUtils.randomBoolean(0.65f)){ // either a hole and a jump
                if (consecutiveRunHoles < 3) {
                    constructRunAndJump(MathUtils.randomBoolean(0.2f), MathUtils.randomBoolean());
                } else {
                    if (MathUtils.randomBoolean()){
                        constructOneHole();
                    } else {
                        constructTwoJumps();
                    }
                }
            } else { // or two jumps
                constructTwoJumps();
            }

        }
    }

    private void constructTwoJumps() {
        Rectangle duckJumpRect = new Rectangle(0, floorLevel, COLLISION_RECTANGLE_WIDTH, DUCK_JUMP_HOLE_HEIGHT);
        //Rectangle cowJumpRect = new Rectangle(0, COW_JUMP_HOLE_HEIGHT + floorLevel + COW_HOLE_SIZE, COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT);

        collisionShapes.add(duckJumpRect);
        //collisionShapes.add(cowJumpRect);

        // Middle wall
        if (MathUtils.randomBoolean(0.9f)) {
            float bottom = floorLevel + DUCK_JUMP_HOLE_HEIGHT + DUCK_HOLE_SIZE;
             float height = (COW_JUMP_HOLE_HEIGHT + floorLevel) - bottom;
            collisionShapes.add(new Rectangle(0, bottom, COLLISION_RECTANGLE_WIDTH, height));
        }

        consecutiveRunHoles = 0;
        consecutiveDuckable++;
    }

    private void constructRunAndJump(boolean cowRun, boolean cowJump) {
        float jumpTop, middleWallHeight, runTop;

        if (cowJump) {
            cowRun = false;
        }

        if (cowRun){
            runTop = COW_RUN_HOLE_HEIGHT + floorLevel;
        } else {
            runTop = DUCK_RUN_HOLE_HEIGHT + floorLevel;
        }

        if (cowJump){
            jumpTop = COW_JUMP_HOLE_HEIGHT + COW_HOLE_SIZE + floorLevel;
            middleWallHeight = COW_JUMP_HOLE_HEIGHT + floorLevel - runTop;
        } else {
            jumpTop = DUCK_JUMP_HOLE_HEIGHT + DUCK_HOLE_SIZE + floorLevel;
            middleWallHeight = DUCK_JUMP_HOLE_HEIGHT + floorLevel - runTop;
        }

        collisionShapes.add(new Rectangle(0, runTop, COLLISION_RECTANGLE_WIDTH, middleWallHeight));
        collisionShapes.add(new Rectangle(0, jumpTop, COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT));

        consecutiveRunHoles++;
        consecutiveDuckable++;
    }

    private void constructOneHole() {
        int holeType;

        if (consecutiveDuckable > 3) {
            holeType = COW_JUMP;
        } else if (consecutiveDuckable > 2) {
            holeType = MathUtils.random(DUCK_JUMP, COW_JUMP);
        } else if (consecutiveCowable < 4) {
            if (MathUtils.randomBoolean()) {
                holeType = DUCK_JUMP;
            } else {
                holeType = DUCK_RUN;
            }
        } else {
            if (consecutiveRunHoles < 2) {
                holeType = MathUtils.random(WALL_OPTIONS);
            } else {
                holeType = MathUtils.random(2, WALL_OPTIONS);
            }
        }


        switch (holeType){
            case DUCK_RUN:
                collisionShapes.add(new Rectangle(0, DUCK_RUN_HOLE_HEIGHT + floorLevel, COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT));
                consecutiveRunHoles++;
                consecutiveDuckable++;
                break;
            case COW_RUN:
                collisionShapes.add(new Rectangle(0, COW_RUN_HOLE_HEIGHT + floorLevel, COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT));
                consecutiveRunHoles++;
                consecutiveDuckable++;
                consecutiveCowable++;
                break;
            case DUCK_JUMP:
                collisionShapes.add(new Rectangle(0, floorLevel, COLLISION_RECTANGLE_WIDTH, DUCK_JUMP_HOLE_HEIGHT));
                collisionShapes.add(new Rectangle(0, DUCK_JUMP_HOLE_HEIGHT + floorLevel + DUCK_HOLE_SIZE, COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT));
                consecutiveRunHoles = 0;
                consecutiveDuckable++;
                break;
            case COW_JUMP:
                collisionShapes.add(new Rectangle(0, floorLevel, COLLISION_RECTANGLE_WIDTH, COW_JUMP_HOLE_HEIGHT));
                //collisionShapes.add(new Rectangle(0, COW_JUMP_HOLE_HEIGHT + floorLevel + COW_HOLE_SIZE, COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT));
                consecutiveRunHoles = 0;
                consecutiveCowable++;
                break;
        }
    }

    public void setPosition(float x){
        for (Rectangle rectangle : collisionShapes){
            rectangle.setX(x);
        }
    }

    public void drawDebug(ShapeRenderer shapeRenderer){
        for (Rectangle rectangle : collisionShapes){
            shapeRenderer.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        }
    }

    public void draw(ShapeRenderer shapeRenderer){

        if (isBest) {
            shapeRenderer.setColor(Color.GOLDENROD);
        } else {
            shapeRenderer.setColor(Color.FIREBRICK);
        }

        for (Rectangle rectangle : collisionShapes){
            shapeRenderer.rect(rectangle.x, rectangle.y, rectangle.width * 3, rectangle.height);
        }
    }

    public void drawTrophy(SpriteBatch batch){
        if (isBest){
            batch.draw(trophyTexture, getX() + getWidth() + 10, floorLevel);
        }
    }

    public void update(float delta){
        setPosition(collisionShapes.get(0).x - (MOVEMENT_SPEED * delta));
    }

    public static void addMovementSpeed() {
        MOVEMENT_SPEED += MOVEMENT_SPEED_INCREMENT;
    }

    public float getWidth() {
        return COLLISION_RECTANGLE_WIDTH;
    }

    public float getX() {
        return collisionShapes.get(0).x;
    }

    public boolean didAnimalCollide(CowDuck cowDuck){
        Circle animalCollision = cowDuck.getCollisionShape();

        boolean collided = false;

        for (Rectangle rectangle : collisionShapes){
            collided |= Intersector.overlaps(animalCollision, rectangle);
        }

        return collided;
    }

    public boolean pointClaimed(){
        return pointClaimed;
    }

    public void claimPoint(){
        pointClaimed = true;
    }

    public void setBest(boolean best) {
        isBest = best;
    }
}
