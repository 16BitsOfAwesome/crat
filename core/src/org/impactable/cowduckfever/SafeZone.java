package org.impactable.cowduckfever;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;

/**
 * Created by James on 4/16/2016.
 */
public class SafeZone {
    private Array<Float> verticalMargins = new Array<Float>();
    private Array<Float> horizontalMargins = new Array<Float>();

    private float width, height;

    private static final float UNIT_SIZE = GameScreen.UNIT_SIZE;

    public SafeZone(float width, float height) {
        this.width = width;
        this.height = height;

        verticalMargins.addAll(UNIT_SIZE, UNIT_SIZE * 2, width - UNIT_SIZE, width - (UNIT_SIZE * 2));
        horizontalMargins.addAll(UNIT_SIZE, UNIT_SIZE * 2, height - UNIT_SIZE, height - (UNIT_SIZE * 2));
    }

    public void draw(ShapeRenderer shapeRenderer){
        for (Float vertical : verticalMargins){
            shapeRenderer.line(vertical, 0, vertical, height, Color.CYAN, Color.CYAN);
        }

        for (Float horizontal : horizontalMargins){
            shapeRenderer.line(0, horizontal, width, horizontal, Color.CYAN, Color.CYAN);
        }
    }
}
