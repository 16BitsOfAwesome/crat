package org.impactable.cowduckfever;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

import static org.impactable.cowduckfever.Animal.COW;

/**
 * Created by James on 4/16/2016.
 */
public class CowDuck {
    public static final float COW_COLLISION_RADIUS = GameScreen.UNIT_SIZE / 3f;
    public static final float DUCK_COLLISION_RADIUS = GameScreen.UNIT_SIZE / 5f;

    private static final float GRAVITY = -GameScreen.UNIT_SIZE; // units per second
    private static final float COW_JUMP_SPEED = 30f; // pixels per second
    private static final float DUCK_JUMP_SPEED = 20f; // pixels per second
    private static final float FRAME_DURATION = 0.05f;

    private Animal currentState;
    private final Circle collisionShape;

    protected boolean airborne = true; // we start foo in the air
    private final Vector2 airbornVelocity = new Vector2();
    private final Vector2 gravity = new Vector2(0, GRAVITY);
    private final Vector2 airResistance = new Vector2(0, 0);

    public static final int COW_TILE_WIDTH = 64;
    public static final int COW_TILE_HEIGHT = 64;
    public static final int DUCK_TILE_WIDTH = 64;
    public static final int DUCK_TILE_HEIGHT = 64;

    float textureX, textureY;

    private final Animation<TextureRegion> cowAnimation;
    private final Animation<TextureRegion> duckAnimation;
    private float animationTimer = 0;

    public CowDuck(Texture cowTexture, Texture duckTexture) {
        TextureRegion[][] region = new TextureRegion(cowTexture).split(COW_TILE_WIDTH, COW_TILE_HEIGHT);
        cowAnimation = new Animation<>(FRAME_DURATION, region[0][0], region[0][1], region[0][2], region[0][3], region[0][4], region[0][5], region[0][6], region[0][7]);
        cowAnimation.setPlayMode(Animation.PlayMode.LOOP);

        region = new TextureRegion(duckTexture).split(DUCK_TILE_WIDTH, DUCK_TILE_HEIGHT);
        duckAnimation = new Animation<>(FRAME_DURATION, region[0][0], region[0][1], region[0][2], region[0][3], region[0][4], region[0][5], region[0][6]);
        duckAnimation.setPlayMode(Animation.PlayMode.LOOP);

        collisionShape = new Circle(0, 0, COW_COLLISION_RADIUS);

        currentState = COW;
        updateShape();
    }

    public void draw(SpriteBatch batch){
        if (currentState == COW) {
            TextureRegion region = (TextureRegion) cowAnimation.getKeyFrame(animationTimer);

            textureX = collisionShape.x - (region.getRegionWidth() / 2f) - 5f;
            textureY = collisionShape.y - (region.getRegionHeight() / 2f) - 5f;

            batch.draw(region, textureX, textureY, COW_TILE_WIDTH * 1.2f, COW_TILE_HEIGHT * 1.2f);
        } else {
            TextureRegion region = (TextureRegion) duckAnimation.getKeyFrame(animationTimer);

            textureX = collisionShape.x - (region.getRegionWidth() / 2f) + 12;
            textureY = collisionShape.y - (region.getRegionHeight() / 2f) + 12;

            batch.draw(region, textureX, textureY, DUCK_TILE_WIDTH * 0.75f, DUCK_TILE_HEIGHT * 0.75f);
        }
    }

    public void drawDebug(ShapeRenderer shapeRenderer){
        shapeRenderer.circle(collisionShape.x, collisionShape.y, collisionShape.radius);
    }

    public void setPosition(float x, float y){
        collisionShape.setX(x);
        collisionShape.setY(y);
    }

    public void update(float delta) {
        animationTimer += delta;

        if (airborne) {
            airbornVelocity.mulAdd(gravity, delta);
            airbornVelocity.mulAdd(airResistance, delta);
        }

        setPosition(collisionShape.x, collisionShape.y + airbornVelocity.y);
    }

    public void jump(){
        if (airborne) return;

        if (currentState == COW) {
            airbornVelocity.set(0, COW_JUMP_SPEED);
            airResistance.set(0, -10);
        } else {
            airbornVelocity.set(0, DUCK_JUMP_SPEED);
            airResistance.set(0, 10);
        }

        airborne = true;
    }

    public Circle getCollisionShape() {
        return collisionShape;
    }

    public void resetPosition(float y) {
        collisionShape.setY(y + collisionShape.radius);
        airborne = false;
        airbornVelocity.set(0, 0);
    }

    public float getPositionY() {
        return collisionShape.y;
    }

    public void shapeShift(){
        if (currentState == COW){
            currentState = Animal.DUCK;
        } else {
            currentState = COW;
        }

        updateShape();
    }

    private void updateShape() {
        if (currentState == COW){
            collisionShape.setRadius(COW_COLLISION_RADIUS);
        } else {
            collisionShape.setRadius(DUCK_COLLISION_RADIUS);
        }
    }

    public float getX() {
        return collisionShape.x;
    }
}
